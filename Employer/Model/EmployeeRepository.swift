//
//  EmployeeRepository.swift
//  Employer
//
//  Created by Kolibaba, Volodymyr on 08.07.19.
//  Copyright © 2019 Kolibaba, Volodymyr. All rights reserved.
//

import Foundation
import Alamofire

class EmployeeRepository {

    typealias EmployeeRepositoryResult<T> = Result<T, EmployeeRepositoryError>
    typealias EmployeeRepositoryCompletionHandler<T> = (EmployeeRepositoryResult<T>) -> Void

    static var networking: Networking.Type = AlamofireNetworking.self

    static func login(
        username: String,
        password: String,
        completionHandler: @escaping (EmployeeRepositoryResult<Void>) -> Void
    ) {

        networking.request(
            Router.login(
                username: username,
                password: password
            )
        ) { result in
            completionHandler(mapResult(result))
        }
    }

    static func allEmployees(
        completionHandler: @escaping (EmployeeRepositoryResult<[Employee]>) -> Void
    ) {
        
        networking.requestData(Router.allEmployees) { result in
            
            switch result {
            case .success(let data):
                
                let decoder = JSONDecoder()
                do {
                    let resources = try decoder.decode([Employee].self, from: data)
                    let employees = resources
                    completionHandler(.success(employees))
                    
                } catch let decodingError as DecodingError {
                    completionHandler(
                        .failure(EmployeeRepositoryError.decodingError(decodingError))
                    )
                } catch {
                    completionHandler(.failure(.unknown(error)))
                }
                
            case .failure(let error):
                completionHandler(.failure(EmployeeRepositoryError(error)))
            }
        }
    }
    
    static func addEmployee(
        _ employee: Employee,
        completionHandler: @escaping (EmployeeRepositoryResult<Employee>) -> Void
    ) {
        
        networking.requestData(Router.addEmployee(employee: employee)) { result in
            
            switch result {
            case .success(let data):
                do {
                    let employee = try JSONDecoder().decode(Employee.self, from: data)
                    completionHandler(.success(employee))
                    
                } catch let decodingError as DecodingError {
                    completionHandler(
                        .failure(EmployeeRepositoryError.decodingError(decodingError))
                    )
                } catch {
                    completionHandler(.failure(.unknown(error)))
                }
            case .failure(let error):
                completionHandler(.failure(EmployeeRepositoryError(error)))
            }
        }
    }
    
    static func removeEmployee(
        _ employee: Employee,
        completionHandler: @escaping (EmployeeRepositoryResult<Void>) -> Void
    ) {
        networking.request(Router.removeEmployee(employee: employee)) { result in
            completionHandler(mapResult(result))
        }
    }
    
    static func updateEmployee(
        _ employee: Employee,
        completionHandler: @escaping (EmployeeRepositoryResult<Void>) -> Void
    ) {
        networking.request(
            Router.updateEmployee(employee: employee)
        ) { result in
            completionHandler(mapResult(result))
        }
    }
}

private extension EmployeeRepository {

    /**
     Maps a result from the Alamofire networking library to a result specific to the EmployeeRepository.
     The `mapResult` function takes a `Result` object from the Alamofire networking library and converts it into an `EmployeeRepositoryResult`.
     This mapping allows for a consistent error handling mechanism within the EmployeeRepository module.
     - Parameter originalResult: The original `Result` object from Alamofire with a generic type `T`, representing either a success with a value of type `T` or a failure with an `AFError`.
     - Returns: An `EmployeeRepositoryResult` containing either the success value of type `T` or an `EmployeeRepositoryError` based on the mapping of the original result.
    **/
    static func mapResult<T>(_ originalResult: Result<T, AFError>) -> EmployeeRepositoryResult<T> {
        switch originalResult {
        case .success(let genericResult):
            return .success(genericResult)
        case .failure(let alamofireError):
            return .failure(EmployeeRepositoryError(alamofireError))
        }
    }
}
