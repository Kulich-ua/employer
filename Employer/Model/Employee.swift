//
//  Employee.swift
//  Employer
//
//  Created by Kolibaba, Volodymyr on 08.07.19.
//  Copyright © 2019 Kolibaba, Volodymyr. All rights reserved.
//

import Foundation

struct Employee: Equatable, Codable {
    
    static let listContentIdentifier = "employeeList"
    
    let id: Int
    let firstName: String
    let lastName: String
    let role: String
    
    var fullName: String {
        
        return firstName + " " + lastName
    }
    
    init(id: Int = -1, firstName: String, lastName: String, role: String) {
        
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.role = role
    }
}
