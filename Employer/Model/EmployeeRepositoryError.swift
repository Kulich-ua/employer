//
//  EmployeeRepositoryError.swift
//  Employer
//
//  Created by Walters, Arne on 19.07.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Alamofire

enum EmployeeRepositoryError: Error {
    /// Unknown errors
    case unknown(Error)
    /// Networking error containing the original AlamofireError
    case networkingError(AFError)
    /// Decoding error containing the original Error
    case decodingError(DecodingError)

    /// Initialize EmployeeRepositoryError from AFError
    init(_ from: AFError) {
        self = .networkingError(from)
    }

    var localizedDescription: String {
        switch self {
        case .unknown(let error):
            return error.localizedDescription
        case .networkingError(let alamofireError):
            return alamofireError.localizedDescription
        case .decodingError(let error):
            return error.localizedDescription
        }
    }
}
