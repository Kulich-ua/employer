//
//  Router.swift
//  Employer
//
//  Created by Kolibaba, Volodymyr on 19.06.19.
//  Copyright © 2019 Kolibaba, Volodymyr. All rights reserved.
//

import Alamofire
import Foundation

enum Router {
    
    case login(username: String, password: String)
    case allEmployees
    case addEmployee(employee: Employee)
    case removeEmployee(employee: Employee)
    case updateEmployee(employee: Employee)

    static let baseURLString = "http://localhost:8080"
    
    private var method: HTTPMethod {
        switch self {
        case .login:
            return .post
        case .allEmployees:
            return .get
        case .addEmployee:
            return .post
        case .removeEmployee:
            return .delete
        case .updateEmployee:
            return .put
        }
    }
    
    private var path: String {
        switch self {
        case .login:
            return "/login"
        case .allEmployees:
            return "/employees"
        case .addEmployee:
            return"/employees"
        case .removeEmployee(let employee):
            return"/employees/\(employee.id)"
        case .updateEmployee(let employee):
            return"/employees/\(employee.id)"
        }
    }
}

// MARK: URLRequestConvertible
extension Router: URLRequestConvertible {
    
    func asURLRequest() throws -> URLRequest {
        
        let url = try Router.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .login(let username, let password):
            
            let parametrs = ["username": username, "password": password]
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parametrs)
            
        case .allEmployees:
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
            
        case .addEmployee(let employee):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
            urlRequest.httpBody = try JSONEncoder().encode(employee)
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
        case .removeEmployee:
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
            
        case .updateEmployee(let employee):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
            urlRequest.httpBody = try JSONEncoder().encode(employee)
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        
        return urlRequest
    }
}
