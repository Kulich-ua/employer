//
//  Networking.swift
//  Employer
//
//  Created by Kolibaba, Volodymyr on 17.07.19.
//  Copyright © 2019 AMS. All rights reserved.
//

import Foundation
import Alamofire

protocol Networking {

    static func requestData(_ urlRequest: URLRequestConvertible, completionHandler: @escaping (Result<Data, AFError>) -> Void)
    static func request(_ urlRequest: URLRequestConvertible, completionHandler: @escaping (Result<Void, AFError>) -> Void)
}

extension Networking {
    
    static func request(_ urlRequest: URLRequestConvertible, completionHandler: @escaping (Result<Void, AFError>) -> Void) {
        
        requestData(urlRequest) { result in
            
            switch result {
            case .success: completionHandler(.success(()))
            case .failure(let error): completionHandler(.failure(error))
            }
        }
    }
}

struct AlamofireNetworking: Networking {
    
    static func requestData(_ urlRequest: URLRequestConvertible, completionHandler: @escaping (Result<Data, AFError>) -> Void) {
        
        AF.request(urlRequest)
            .validate()
            .responseData { response in
                completionHandler(response.result)
            }
    }
}
