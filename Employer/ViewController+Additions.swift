//
//  ViewController+Additions.swift
//  Employer
//
//  Created by Kolibaba, Volodymyr on 09.07.19.
//  Copyright © 2019 Kolibaba, Volodymyr. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(title: String, message: String? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default))
        present(alertController, animated: true)
    }
}
