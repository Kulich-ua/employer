//
//  EmployeeTableViewCell.swift
//  Employer
//
//  Created by Kolibaba, Volodymyr on 09.07.19.
//  Copyright © 2019 Kolibaba, Volodymyr. All rights reserved.
//

import UIKit

class EmployeeTableViewCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var roleLabel: UILabel!
    
    static var identifier: String {
        
        return String(describing: self)
    }
    
    var employee: Employee? {
        
        didSet {
            
            nameLabel.text = employee?.fullName
            roleLabel.text = employee?.role
        }
    }
}
