//
//  EmployeeListViewController.swift
//  Employer
//
//  Created by Kolibaba, Volodymyr on 18.06.19.
//  Copyright © 2019 Kolibaba, Volodymyr. All rights reserved.
//

import UIKit

class EmployeeListViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    private lazy var refreshControl: UIRefreshControl = {
        
        let refreshControl = UIRefreshControl()
        
        refreshControl.backgroundColor = UIColor(white: 0.98, alpha: 1.0)
        refreshControl.tintColor = UIColor.darkGray
        
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching employees data ...")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        return refreshControl
    }()
    
    private var employees = [Employee]() {
        
        didSet {            
            tableView.reloadData()
            tableView.refreshControl?.endRefreshing()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.refreshControl = refreshControl
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refresh()
    }
    
    @objc private func refresh() {
        
        EmployeeRepository.allEmployees { result in
            
            switch result {
            case .success(let employees): self.employees = employees
            case .failure(let error): self.showAlert(title: "Failed to load employees!", message: error.localizedDescription)
            }
        }
    }
    
     // MARK: - Navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        guard
            let employeeViewController = segue.destination as? EmployeeViewController,
            let employeeCell = sender as? EmployeeTableViewCell,
            let employee = employeeCell.employee else {
            
            return
        }
        employeeViewController.employee = employee
     }
}

extension EmployeeListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            let employee = employees[indexPath.row]
            EmployeeRepository.removeEmployee(employee) { result in
                
                switch result {
                case .success(): self.employees.remove(at: indexPath.row)
                case .failure(let error): self.showAlert(title: "Failed to remove employee!", message: error.localizedDescription)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return employees.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: EmployeeTableViewCell.identifier, for: indexPath)
        
        guard let employeeCell = cell as? EmployeeTableViewCell else {
            
            fatalError("Unable to dequeueReusable EmployeeTableViewCell!")
        }
        
        employeeCell.employee = employees[indexPath.row]
        
        return employeeCell
    }
}

extension EmployeeListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        tableView.deselectRow(at: indexPath, animated: true)
        
        let cell = tableView.cellForRow(at: indexPath)
        performSegue(withIdentifier: "showEmployee", sender: cell)
    }
}
