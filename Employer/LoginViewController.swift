//
//  LoginViewController.swift
//  Employer
//
//  Created by Kolibaba, Volodymyr on 08.07.19.
//  Copyright © 2019 Kolibaba, Volodymyr. All rights reserved.
//

import UIKit
import Alamofire


class LoginViewController: UIViewController {
    
    @IBOutlet private weak var usernameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var loginButton: UIButton!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #warning("Remove this for production!")
        usernameTextField.text = "admin"
        passwordTextField.text = "adminPass"        
    }
    
    @IBAction private func login() {
        
        guard
            let username = usernameTextField.text, !username.isEmpty,
            let password = passwordTextField.text, !password.isEmpty else {
                
                showAlert(title: "Please enter username and password")
                return
        }
        
        startActivity()
        
        EmployeeRepository.login(username: username, password: password) { result in
            
            switch result {
            case .success:
                
                self.showEmployees()
                
            case .failure(let error):
                
                self.showAlert(title: "Failed to log in", message: error.localizedDescription)
            }
            
            self.stopActivity()
        }
    }
    
    private func showEmployees() {
        
        performSegue(withIdentifier: "showEmployees", sender: self)
    }
    
    private func startActivity() {
     
        activityIndicator.startAnimating()
        loginButton.isHidden = true
    }
    
    private func stopActivity() {
        
        activityIndicator.stopAnimating()
        loginButton.isHidden = false
    }
}
