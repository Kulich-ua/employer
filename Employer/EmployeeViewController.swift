//
//  EmployeeViewController.swift
//  Employer
//
//  Created by Kolibaba, Volodymyr on 16.07.19.
//  Copyright © 2019 AMS. All rights reserved.
//

import UIKit

class EmployeeViewController: UIViewController {
    
    @IBOutlet private weak var firstNameTextField: UITextField!
    @IBOutlet private weak var lastNameTextField: UITextField!
    @IBOutlet private weak var roleTextField: UITextField!
    @IBOutlet private weak var actionButton: UIButton!
    
    var employee: Employee?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyEmployee(employee)
    }
    
    @IBAction private func action() {
    
        guard let employee = employee else {
            
            createEmployee()
            return
        }
        updateEmployee(employee)
    }
    
    private func makeEmployee(id: Int? = nil) ->  Employee? {
        
        guard
            let firstName = firstNameTextField.text, !firstName.isEmpty,
            let lastName = lastNameTextField.text, !lastName.isEmpty,
            let role = roleTextField.text, !role.isEmpty else {
                
                self.showAlert(title: "Please enter the full name and role")
                return nil
        }
        
        guard let id = id else {
            
            return Employee(firstName: firstName, lastName: lastName, role: role)
        }
        
        return Employee(id: id, firstName: firstName, lastName: lastName, role: role)
    }
    
    private func createEmployee() {
        
        guard let newEmployee = makeEmployee() else {
            
            return
        }
        EmployeeRepository.addEmployee(newEmployee) { result in
            
            switch result {
            case .success( _): self.navigationController?.popViewController(animated: true)
            case .failure(let error): self.showAlert(title: "Failed to create a new employee!", message: error.localizedDescription)
            }
        }
    }
    
    private func updateEmployee(_ employee: Employee) {
        
        guard let newEmployee = makeEmployee(id: employee.id) else {
            
            return
        }
        EmployeeRepository.updateEmployee(newEmployee) { result in
            
            switch result {
            case .success(): self.navigationController?.popViewController(animated: true)
            case .failure(let error): self.showAlert(title: "Failed to create a new employee!", message: error.localizedDescription)
            }
        }
    }
    
    private func applyEmployee(_ employee: Employee?) {
        
        firstNameTextField.text = employee?.firstName
        lastNameTextField.text = employee?.lastName
        roleTextField.text = employee?.role
        
        if employee == nil {
            
            title = "New Employee"
            actionButton.setTitle("Create", for: .normal)
            
        } else {
            
            title = "Update Employee"
            actionButton.setTitle("Update", for: .normal)
        }
    }
}
