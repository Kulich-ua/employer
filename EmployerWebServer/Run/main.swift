//
//  main.swift
//  EmployerWebServer
//
//  Created by Rodney, Roy on 23.02.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Vapor

let app = Application(.development)
let backend = BackendInMemory()

defer { app.shutdown() }
try configure(app, backend)
try app.run()

