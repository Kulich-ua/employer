//
//  configure.swift
//  EmployerWebServer
//
//  Created by Rodney, Roy on 23.02.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Vapor

func configure(_ app: Application, _ backend: BackendProtocol) throws {
    app.middleware.use(app.sessions.middleware)
    routes(app, backend)
}
