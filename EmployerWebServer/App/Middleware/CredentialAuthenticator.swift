//
//  CredentialAuthenticator.swift
//  EmployerWebServer
//
//  Created by Rodney, Roy on 23.02.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Vapor

/// Authenticates request using credential in request content.
/// Saves credential to session.
struct CredentialAuthenticator: AsyncRequestAuthenticator {
    var backend: BackendProtocol
    
    func authenticate(request req: Vapor.Request) async throws {
        let credential = try req.content.decode(Credential.self)
        if await backend.mayLogin(with: credential) {
            req.session.authenticate(credential)
            req.auth.login(credential)
        }
    }
    
}
    
    
    
//    func authenticate(
//        basic: BasicAuthorization,
//        for request: Request
//    ) async throws {
//        if basic.username == "test" && basic.password == "secret" {
//            request.auth.login(User(name: "Vapor"))
//        }
//   }

