//
//  CredentialSessionAuthenticator.swift
//  EmployerWebServer
//
//  Created by Rodney, Roy on 23.02.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Foundation
import Vapor

/// Authenticates request using credentials save in session.
struct UserSessionAuthenticator: AsyncSessionAuthenticator {
    typealias User = Credential
    
    var backend: BackendProtocol
    
    func authenticate(sessionID: String, for request: Request) async throws {
        guard let authenticatedSessionId = request.session.authenticated(User.self)
        else { return }
        
        if let user = await backend.getCredential(forSessionID: authenticatedSessionId)
        { request.auth.login(user) }
    }
}
