//
//  putEmployeeRoute.swift
//  EmployerWebServer
//
//  Created by Rodney, Roy on 24.02.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Foundation
import Vapor

/// PUT employee 
/// /employees/:id
/// Returns 204
func putEmployeeRoute(protected: RoutesBuilder, backend: BackendProtocol) {
    protected.put("employees", ":id") { req async throws in
        guard let id = req.parameters.get("id", as: Int.self) else {
            throw Abort(.badRequest)
        }
        
        var updatedEmployee = try req.content.decode(Employee.self)
        updatedEmployee.id = id
        
        try await backend.updateOrCreateEmployee(updatedEmployee)
        return HTTPStatus.noContent
    }
}
