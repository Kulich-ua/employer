//
//  postEmployeeRoute.swift
//  EmployerWebServer
//
//  Created by Rodney, Roy on 24.02.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Foundation
import Vapor

/// POST New employee
/// /employees
/// Returns 200 Employee
func postEmployeeRoute(protected: RoutesBuilder, backend: BackendProtocol) {
    protected.post("employees") { req async throws in
        let newEmployee = try req.content.decode(Employee.self)
        return await backend.addEmployee(newEmployee)
    }
}
