//
//  getEmployeeRoute.swift
//  EmployerWebServer
//
//  Created by Rodney, Roy on 24.02.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Foundation
import Vapor

/// GET Employee
/// /employees/id
/// Returns 200 Employee
func getEmployeeRoute(protected: RoutesBuilder, backend: BackendProtocol) {
    protected.get("employees", ":id") { req async throws in
        guard let id = req.parameters.get("id", as: Int.self) else {
            throw Abort(.badRequest)
        }
        guard let employee = await backend.getEmployee(withId: id) else {
            throw Abort(.notFound)
        }
        return employee
    }
}
