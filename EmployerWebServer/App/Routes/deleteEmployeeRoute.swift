//
//  deleteEmployeeRoute.swift
//  EmployerWebServer
//
//  Created by Rodney, Roy on 24.02.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Foundation
import Vapor

/// Delete an employee
/// /employees/:id
/// Returns 204
func deleteEmployeeRoute(protected: RoutesBuilder, backend: BackendProtocol) {
    protected.delete("employees", ":id") { req async throws in
        guard let id = req.parameters.get("id", as: Int.self) else {
            throw Abort(.badRequest)
        }
        try await backend.deleteEmployee(id)
        return HTTPStatus.noContent
    }
}
