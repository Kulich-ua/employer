//
//  loginRoute.swift
//  EmployerWebServer
//
//  Created by Rodney, Roy on 24.02.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Foundation
import Vapor


/// POST Login
/// /login
/// Returns 204
func loginRoute(_ app: Application, _ backend: BackendProtocol) {
    app.grouped(
        CredentialAuthenticator(backend: backend),
        Credential.guardMiddleware()
    ).post("login") { req in
        return HTTPStatus.noContent
    }
}
