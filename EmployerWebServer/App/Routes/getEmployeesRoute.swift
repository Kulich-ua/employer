//
//  getEmployeesRoute.swift
//  EmployerWebServer
//
//  Created by Rodney, Roy on 24.02.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Foundation
import Vapor

/// GET All employees
/// /employees
/// Returns 200 [Employee]
func getEmployeesRoute(protected: RoutesBuilder, backend: BackendProtocol) {
    protected.get("employees") { req async in
        return await backend.getAllEmployees()
    }
}
