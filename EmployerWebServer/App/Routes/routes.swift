//
//  routes.swift
//  EmployerWebServer
//
//  Created by Rodney, Roy on 23.02.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Vapor

func routes(_ app: Application, _ backend: BackendProtocol) {
    
    loginRoute(app, backend)
    
    let protected = app.grouped(
        UserSessionAuthenticator(backend: backend),
        Credential.guardMiddleware()
    )
    
    getEmployeesRoute(protected: protected, backend: backend)
    postEmployeeRoute(protected: protected, backend: backend)
    deleteEmployeeRoute(protected: protected, backend: backend)
    getEmployeeRoute(protected: protected, backend: backend)
    putEmployeeRoute(protected: protected, backend: backend)
    getEmployeeRoute(protected: protected, backend: backend)
}
