//
//  BackendProtocol.swift
//  EmployerWebServer
//
//  Created by Rodney, Roy on 23.02.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Foundation

protocol BackendProtocol {
    
    func getEmployee(withId: Int) async -> Employee?
    func getAllEmployees() async -> [Employee]
    
    /// Assigns new ID and adds employee.
    func addEmployee(_ : Employee) async -> Employee
    
    /// Since this is a PUT request, it must be idempotent.
    /// Therefore, a user with no ID will not be accepted.
    /// - Parameter updatedEmployee: Overwrites employee with ID, or adds employee if none exists.
    func updateOrCreateEmployee(_ : Employee) async throws
    
    /// Throws if employee with ID does not exist.
    func deleteEmployee(_ : Employee.ID) async throws
        
    func mayLogin(with: Credential) async -> Bool
    
    func getCredential(forSessionID: String) async -> Credential?
    
}

enum BackendError : Error {
    case missingIdOnPutRequest
    case idToDeleteMissing
}
