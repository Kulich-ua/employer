//
//  BackendInMemory.swift
//  EmployerWebServer
//
//  Created by Rodney, Roy on 23.02.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Foundation

actor BackendInMemory {
    var employees: [Employee] = Employee.getTestEmployees()
    var credentials: Set<Credential> = Credential.getTestCredentials()
}

extension BackendInMemory : BackendProtocol {
    
    func getEmployee(withId id: Int) -> Employee? {
        return employees.first{$0.id == id}
    }
    
    func getAllEmployees() -> [Employee] {
        return employees
    }
    
    func addEmployee(_ newEmployee: Employee) -> Employee {
        var newEmployee = newEmployee
        
        // make new Id by incrementing highest present
        newEmployee.id = 1 + employees.reduce(0, { partialResult, employee in
            max((employee.id ?? 0), partialResult)
        })
        employees.append(newEmployee)
        return newEmployee
    }
    
    func deleteEmployee(_ id: Employee.ID) throws {
        guard let index = employees.firstIndex(where: {$0.id == id}) else {
            throw BackendError.idToDeleteMissing
        }
        
        employees.remove(at: index)
    }
    
    func updateOrCreateEmployee(_ updatedEmployee: Employee) throws {
        guard let updatedEmployeeId = updatedEmployee.id else {
            throw BackendError.missingIdOnPutRequest
        }
        
        if let updateIndex = employees.firstIndex(where: {$0.id == updatedEmployeeId}) {
            employees[updateIndex] = updatedEmployee
        } else {
            employees.append(updatedEmployee)
        }
    }
    
    func mayLogin(with credential: Credential) -> Bool {
        credentials.contains(credential)
    }
    
    func getCredential(forSessionID sessionID: String) async -> Credential? {
        return credentials.first{$0.sessionID == sessionID}
    }
}
