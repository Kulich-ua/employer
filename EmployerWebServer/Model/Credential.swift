//
//  Credential.swift
//  EmployerWebServer
//
//  Created by Rodney, Roy on 23.02.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Foundation
import Vapor

struct Credential : Codable, Hashable {
    var username: String
    var password: String
}

extension Credential: SessionAuthenticatable {
    var sessionID: String { self.username }
}

extension Credential {
    static func getTestCredentials() -> Set<Credential> {
        [
            Credential(username: "admin", password: "adminPass")
        ]
    }
}
