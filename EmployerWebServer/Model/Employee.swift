//
//  Employee.swift
//  EmployerWebServer
//
//  Created by Rodney, Roy on 23.02.23.
//  Copyright © 2023 AMS. All rights reserved.
//

import Foundation
import Vapor

struct Employee : Codable, Identifiable {
    var id: Int?
    let firstName: String
    let lastName: String
    let role: String
}

extension Employee : Content {}

extension Employee {
    static func getTestEmployees() -> [Employee] {
        [
        Employee(id: 0, firstName: "Dwight", lastName: "Schrute", role: "Assistant to the Regional Manager"),
        Employee(id: 1, firstName: "Michael", lastName: "Scott", role: "Regional Manager"),
        Employee(id: 2, firstName: "Toby", lastName: "Flenderson", role: "Human Resources Representative")
        ]
    }
}
