//
//  RouterTest.swift
//  EmployerTests
//
//  Created by Kolibaba, Volodymyr on 10.07.19.
//  Copyright © 2019 Kolibaba, Volodymyr. All rights reserved.
//

import XCTest
import Alamofire
@testable import Employer

class RouterTest: XCTestCase {

    func testUpdateEmployees() {
        
        let employee = Employee.test
        let employeeId = employee.id
        
        let request = try? Router.updateEmployee(employee: Employee.test).asURLRequest()
        XCTAssertEqual(request?.url, URL(string: "\(Router.baseURLString)/employees/\(employeeId)"))
        XCTAssertEqual(request?.httpMethod, HTTPMethod.put.rawValue)
        XCTAssertEqual(request?.allHTTPHeaderFields?["Content-Type"], "application/json")
        XCTAssertEqual(request?.httpBody, try JSONEncoder().encode(employee))
    }
    
    func testRemoveEmployees() {
        
        let employee = Employee.test
        let employeeId = employee.id
        
        let allEmployeesRequest = try? Router.removeEmployee(employee: Employee.test).asURLRequest()
        XCTAssertEqual(allEmployeesRequest?.url, URL(string: "\(Router.baseURLString)/employees/\(employeeId)"))
        XCTAssertEqual(allEmployeesRequest?.httpMethod, HTTPMethod.delete.rawValue)
        XCTAssertNil(allEmployeesRequest?.httpBody)
    }

    func testAddEmployees() {
        
        let employee = Employee.test
        let request = try? Router.addEmployee(employee: employee).asURLRequest()
        XCTAssertEqual(request?.url, URL(string: "\(Router.baseURLString)/employees"))
        XCTAssertEqual(request?.httpMethod, HTTPMethod.post.rawValue)
        XCTAssertEqual(request?.allHTTPHeaderFields?["Content-Type"], "application/json")
        XCTAssertEqual(request?.httpBody, try JSONEncoder().encode(employee))
    }

    func testAllEmployees() {

        let request = try? Router.allEmployees.asURLRequest()
        XCTAssertEqual(request?.url, URL(string: "\(Router.baseURLString)/employees"))
        XCTAssertEqual(request?.httpMethod, HTTPMethod.get.rawValue)
    }
    
    func testLogin() {
        
        let login = Router.login(username: "Name", password: "Secrete")
        let loginRequest = try? login.asURLRequest()
        XCTAssertEqual(loginRequest?.url, URL(string: "\(Router.baseURLString)/login"))
        XCTAssertEqual(loginRequest?.httpMethod, HTTPMethod.post.rawValue)
        
        let bodyData = loginRequest!.httpBody!
        let body = String(data: bodyData, encoding: .utf8)
        XCTAssertEqual(body, "password=Secrete&username=Name")
    }
}
