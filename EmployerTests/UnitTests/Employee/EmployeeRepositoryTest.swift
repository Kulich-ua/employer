//
//  EmployeeTest.swift
//  EmployerTests
//
//  Created by Kolibaba, Volodymyr on 10.07.19.
//  Copyright © 2019 Kolibaba, Volodymyr. All rights reserved.
//

import XCTest
import Alamofire
@testable import Employer

let testError = AFError.responseValidationFailed(reason: .dataFileNil)

class MockNetworking: Networking {

    static var data: Data?

    static func requestData(_ urlRequest: Alamofire.URLRequestConvertible, completionHandler: @escaping (Result<Data, AFError>) -> Void) {
        guard let data = data else {

            completionHandler(.failure(testError))
            return
        }
        completionHandler(.success(data))
    }
}

class EmployeeRepositoryTest: XCTestCase {
    
    let standardNetworking = EmployeeRepository.networking
    
    let employee = Employee.test

    override func setUp() {
        
        MockNetworking.data = nil
        EmployeeRepository.networking = MockNetworking.self
    }
    
    override func tearDown() {
        
        EmployeeRepository.networking = standardNetworking
    }
    
    func testLoginSuccess() {
        
        let expectation = self.expectation(description: "Login")
        
        MockNetworking.data = Data()
        
        let successCompletionHandler = EmployeeRepositoryTest.testSuccessCompletionHandler(expectation: expectation)
        EmployeeRepository.login(username: "Name", password: "Secret", completionHandler: successCompletionHandler)
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func testLoginFailure() {
        
        let expectation = self.expectation(description: "Login")
        
        MockNetworking.data = nil
        
        EmployeeRepository.login(username: "Name", password: "Secret") { result in
            
            switch result {
            case .success: XCTFail("Supposed to fail!")
            case .failure(let error):
                switch error {
                case .networkingError(let error):
                    XCTAssertNotNil(error)
                default:
                    XCTFail("Expected networking error")
                }

            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func testAddEmployeeSuccess() {
        
        let expectation = self.expectation(description: "Create new employee")
        
        let employee = Employee(id: 1, firstName: "FirstName", lastName: "LastName", role: "Role")
        MockNetworking.data = try? JSONEncoder().encode(employee)
        
        EmployeeRepository.addEmployee(employee) { result in
            
            switch result {
            case .success(let newEmployee):
                XCTAssertEqual(employee, newEmployee)
            case .failure(let error):
                XCTFail("Unexpected error: \(error.localizedDescription)")
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func testAddEmployeeFailure() {
        
        let expectation = self.expectation(description: "Create new employee")
        
        MockNetworking.data = nil
        
        EmployeeRepository.addEmployee(employee) { result in
            
            switch result {
            case .success:
                XCTFail("Supposed to fail!")
            case .failure(let error):
                switch error {
                case .networkingError(let error):
                    XCTAssertNotNil(error)
                default:
                    XCTFail("Expected networking error")
                }
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func testRemoveEmployee() {
        
        let expectation = self.expectation(description: "Remove employee")
        
        MockNetworking.data = Data()
        
        let successCompletionHandler = EmployeeRepositoryTest.testSuccessCompletionHandler(expectation: expectation)
        EmployeeRepository.removeEmployee(employee, completionHandler: successCompletionHandler)
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func testUpdateEmployee() {
        
        let expectation = self.expectation(description: "Update employee")
        
        MockNetworking.data = Data()
        
        let successCompletionHandler = EmployeeRepositoryTest.testSuccessCompletionHandler(expectation: expectation)
        EmployeeRepository.updateEmployee(employee, completionHandler: successCompletionHandler)
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func testAllEmployeesSucces() {
        
        let expectation = self.expectation(description: "Loading all employees")
        
        MockNetworking.data = EmployeeRepositoryTest.testJSONData(file: "employees")
        
        EmployeeRepository.allEmployees { result in
            
            switch result {
            case .success(let employees):
                XCTAssertEqual(employees.count, 2)
                let jsonEmployee = Employee(id: 1, firstName: "Bilbo", lastName: "Baggins", role: "burglar")
                XCTAssertEqual(employees.first!, jsonEmployee)
                
            case .failure(let error):
                XCTFail("Unexpected error: \(error.localizedDescription)")
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func testAllEmployeesNetworkFailure() {
        
        let expectation = self.expectation(description: "Loading all employees")
        
        MockNetworking.data = nil
        
        EmployeeRepository.allEmployees { result in
            
            switch result {
            case .success:
                XCTFail("Supposed to fail!")
            case .failure(let error):
                switch error {
                case .networkingError(let error):
                    XCTAssertNotNil(error)
                default:
                    XCTFail()
                }
            }
            
            expectation.fulfill()
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func testAllEmployeesDataFailure() {
        
        let expectation = self.expectation(description: "Loading all employees")
        
        MockNetworking.data = Data()
        
        EmployeeRepository.allEmployees { result in
            
            switch result {
            case .success:
                XCTFail("Supposed to fail!")
            case .failure(let error):
                switch error {
                case .decodingError(let decodingError):
                    XCTAssertNotNil(decodingError)
                default:
                    XCTFail("Expected decoding error")
                }
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    static func testJSONData(file: String) -> Data {
        let bundle = Bundle(for: self)
        let url = bundle.url(forResource: file, withExtension: "json")!
        return try! Data(contentsOf: url)
    }
    
    static func testSuccessCompletionHandler(expectation: XCTestExpectation) -> (Result<Void, EmployeeRepositoryError>) -> Void {

        return { result in
            if case let .failure(error) = result {
                XCTFail("Unexpected error: \(error.localizedDescription)")
            }
            expectation.fulfill()
        }
    }
}
