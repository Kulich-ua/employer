//
//  EmployeeRepositoryTest.swift
//  EmployerTests
//
//  Created by Kolibaba, Volodymyr on 16.07.19.
//  Copyright © 2019 AMS. All rights reserved.
//

import XCTest
import Alamofire
@testable import Employer


class EmployeeRepositoryIntegrationTest: XCTestCase {

    private let timeout = 60.0

    func testLoginSuccess() {
        
        let expectation = self.expectation(description: "Login")
        
        MockNetworking.data = Data()
        
        let successCompletionHandler = EmployeeRepositoryTest.testSuccessCompletionHandler(expectation: expectation)
        EmployeeRepository.login(username: "admin", password: "adminPass", completionHandler: successCompletionHandler)
        
        waitForExpectations(timeout: timeout, handler: nil)
    }
    
    func testLoginFailure() {
        
        let expectation = self.expectation(description: "Login")
        
        MockNetworking.data = nil
        
        EmployeeRepository.login(username: "Name", password: "Secret") { result in
            
            switch result {
            case .success: XCTFail("Supposed to fail!")
                
            case .failure(let error):
                switch error {
                case .networkingError(let afError):
                    XCTAssertEqual(afError.responseCode, 401)
                default:
                    XCTFail("401 AFError expected")
                }
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: timeout, handler: nil)
    }
    
    func testAllEmployees() {
        
        testAddEmployeeSuccess()
        
        let expectation = self.expectation(description: "Loading all employees")
        
        EmployeeRepository.allEmployees { result in
            
            switch result {
            case .success(let employees): EmployeeRepositoryIntegrationTest.testEmployee(employees.last!)
            case .failure(let error): XCTFail("Unexpected error: \(error.localizedDescription)")
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: timeout, handler: nil)
    }
    
    func testAddEmployeeSuccess() {
        
        _ = addTestEmployee()
    }
    
    func testRemoveEmployee() {

        let testEmployee = addTestEmployee()

        let expectation = self.expectation(description: "Remove employee")
        
        let successCompletionHandler = EmployeeRepositoryTest.testSuccessCompletionHandler(expectation: expectation)
        EmployeeRepository.removeEmployee(testEmployee, completionHandler: successCompletionHandler)
        
        waitForExpectations(timeout: timeout, handler: nil)
    }
    
    func testUpdateEmployee() {
        
        let newEmployee = addTestEmployee()
        let changedEmployee = Employee(id: newEmployee.id, firstName: "~Name", lastName: "~Last Name", role: "~Role")
        
        let expectation = self.expectation(description: "Update employee")
        
        let successCompletionHandler = EmployeeRepositoryTest.testSuccessCompletionHandler(expectation: expectation)
        EmployeeRepository.updateEmployee(changedEmployee, completionHandler: successCompletionHandler)
        
        waitForExpectations(timeout: timeout, handler: nil)
    }
    
    func addTestEmployee() -> Employee {
        
        testLoginSuccess()
        
        let expectation = self.expectation(description: "Create new employee")
        
        var newEmployee: Employee? = nil
        
        EmployeeRepository.addEmployee(Employee.test) { result in
            
            switch result {
            case .success(let employee):
                EmployeeRepositoryIntegrationTest.testEmployee(employee)
                newEmployee = employee
                
            case .failure(let error):
                XCTFail("Unexpected error: \(error.localizedDescription)")
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: timeout, handler: nil)
        
        return newEmployee!
    }
    
    static func testEmployee(_ employee: Employee) {
        
        let testEmployees = Employee.test
        XCTAssertEqual(employee.fullName, testEmployees.fullName)
        XCTAssertEqual(employee.role, testEmployees.role)
    }
}
