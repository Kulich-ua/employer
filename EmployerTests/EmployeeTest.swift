//
//  EmployeeTest.swift
//  EmployerTests
//
//  Created by Kolibaba, Volodymyr on 17.07.19.
//  Copyright © 2019 AMS. All rights reserved.
//

import Foundation

@testable import Employer

extension Employee {
    
    static var test = Employee(firstName: "Sam", lastName: "One", role: "Manager")
}
